﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiEnemySpawner : MonoBehaviour
{
    public GameObject[] enemyList;
    public Transform[] spawnPointList;
    public bool showGizmos = true;
    private bool enabled = true;
    
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && enabled)
        {
            for (int i = 0; i < enemyList.Length; i++)
            
            {
                int rnd = Random.Range(0, enemyList.Length);
                int rndPostition = Random.Range(0, spawnPointList.Length);
                Instantiate(enemyList[rnd], spawnPointList[rndPostition].position, Quaternion.identity);
                Debug.Log(rnd);
            }
            enabled = false;
        }
    }

    void OnDrawGizmosSelected() {
        // Drawing spawn zone
        if(showGizmos)
        {
            Gizmos.color = Color.red;
            for(int i = 0; i < spawnPointList.Length; i++)
            {
                Gizmos.DrawWireSphere(spawnPointList[i].position, 2);
            }
            
        }
    }
}
