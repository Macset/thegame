﻿using UnityEngine;
using UnityEngine.Events;

public class Weapon : MonoBehaviour
{
    public int damage = 10;
    public float range = 100f;
    public float fireRate = 15f;
    public float force = 30f;
    public Camera fpsCam;
    public UnityEvent shootEvent;

    Animator anim;

    private float nextTimeToFire = 0f;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if(Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
        }
        else if(Input.GetButtonDown("Fire3"))
        {
            anim.SetTrigger("View");
        }
        else
        {   
            anim.SetTrigger("Idle");
        }
    }

    void Shoot()
    {
        shootEvent.Invoke();
        anim.SetTrigger("Fire");
        RaycastHit hit;
        
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            CharacterStats target = hit.transform.GetComponent<CharacterStats>();
            if(target != null)
            {
                target.TakeDamage(damage);  
            }
            
            if(hit.rigidbody != null){
                hit.rigidbody.AddForce(-hit.normal * force);
            }
        }
    }
}
