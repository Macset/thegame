﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    [Header("Details")]
    public string name;
    [Multiline]
    public string desctiption;

    [Header("Stats")]
    public bool isAlive = true;
    public Stat health;

    //public Stat armor;

    public enum CharacterTypes
    {
        Player,
        Enemy
    }

    public CharacterTypes characterType;

    EnemyController controller;
    
    void Start() 
    {
        if(characterType == CharacterTypes.Enemy)
        {
            controller = GetComponent<EnemyController>();
            if(name == null){ name = "Monster"; }
        }
        
    }

    public void TakeDamage(int damage)
    {
        if(isAlive){
            //damage -= armor.getValue();
            damage = Mathf.Clamp(damage, 0, int.MaxValue);

            health.reduceValue(damage);
            Debug.Log(name + " takes " + damage + " damage.");
        }

        if(health.getValue() <= 0)
        {
            isAlive = false;
            controller.Die();
        }
    }
}
