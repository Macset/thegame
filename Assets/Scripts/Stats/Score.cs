﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public static Score instance;

    #region Signeton

    void Awake() {
        instance = this;
    }

    #endregion

    public Text scoreText;
    private int score;

    private void Start() {
        scoreText.text = score.ToString();
    }

    private void Update() {
        scoreText.text = score.ToString();
    }

    public void AddScore(int count)
    {
        score += count;
    }

    public void RemoveScore(int count)
    {
        score -= count;
    }

    public void CleanScore()
    {
        score = 0;
    }

    public int GetScore()
    {
        return score;
    }
}
