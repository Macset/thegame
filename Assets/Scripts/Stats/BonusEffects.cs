﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusEffects : MonoBehaviour
{

    #region Singleton

    public static BonusEffects instance;

    void Awake() {
        instance = this;   
    }

    #endregion


    public int damageBonus;
    public int healthBonus;
}
