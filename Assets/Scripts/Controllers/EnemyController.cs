﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    [Header("Stats")]
    public float lookRadius = 30f;
    public float attackDistance = 5f;
    public float walkSpeed = 5f;
    public float runSpeed = 15f;
    public float rotateMultiplier = 100f;
    [Header("Objects")]
    public Transform target;

    private CharacterStats targetStats;
    private CharacterStats Stats;
    private Animator animator;
    
    private float distance;
    
    NavMeshAgent nav;

    [Header("Dev settings")]
    public bool showGizmos = true;

    
    void Start()
    {
        target = PlayerManager.instance.player.transform;
        nav = GetComponent<NavMeshAgent>();
        targetStats = target.GetComponent<CharacterStats>();
        Stats = GetComponent<CharacterStats>();
        animator = GetComponent<Animator>();
        nav.speed = setSpeed();
        
    }

    
    void Update()
    {
        if(Stats.isAlive)
        {
            distance = Vector3.Distance(target.position, transform.position);
            // Idle animation
            if(!canSee())
            {
                nav.enabled = false;
                animator.SetTrigger("Idle");
            }

            // Run animation and go to player
            if(canSee())
            {
                nav.speed = setSpeed();
                nav.enabled = true;
                animator.SetTrigger("Run");
                nav.SetDestination(target.position);
                
            }

            // Attack animation and attack player
            if(canAttack())
            {
                FaceTarget();
                animator.SetTrigger("Attack");
                nav.enabled = false;
                nav.Stop();
            }
        }
        
    }
    
    void OnDrawGizmosSelected() {
        // Drawing trigger zones
        if(showGizmos)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, lookRadius);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, attackDistance);
        }
    }

    private void FaceTarget ()
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotateMultiplier);
	}
    
    public void Die()
    {        
            animator.SetTrigger("Dead");
            nav.enabled = false; 
    }

    private float setSpeed()
    {
        if(canSee()){return runSpeed;}
        else{return walkSpeed;}
    }

    private bool canSee()
    {
        if(distance <= lookRadius && distance >= attackDistance)
        {
            return true;
        }
        else { return false; }
    }

    private bool canAttack()
    {
        if(distance <= attackDistance)
        {
            return true;
        }
        else{ return false; }
    }
}
